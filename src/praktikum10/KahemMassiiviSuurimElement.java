package praktikum10;

public class KahemMassiiviSuurimElement {

	public static void main(String[] args) {
		
		int[][] neo = {
			    {1, 3, 6, 7},
			    {2, 3, 3, 1},
			    {17, 4, 5, 0},
			    {-20, 13, 16, 17}
			};
		
		int[] suurimad = new int [neo.length];
		
		for (int i = 0; i < neo.length; i++) {
			suurimad[i] = leiaSuurim(neo[i]);
		}
		
		int suurimNr = leiaSuurim(suurimad);
			
		System.out.println(suurimNr);

	}

	private static int leiaSuurim(int[] massiiv) {
		int suurim = Integer.MIN_VALUE;
		
		for (int i = 0; i < massiiv.length; i++) {				
				if (suurim < massiiv[i])
					suurim = massiiv[i];				
			}
			
		return suurim;
	}

}
