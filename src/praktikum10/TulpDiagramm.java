package praktikum10;

import java.util.ArrayList;
import java.util.Arrays;

import lib.TextIO;

public class TulpDiagramm {

	public static void main(String[] args) {
		
		ArrayList<Integer> numbrid = new ArrayList();
		int suurim = Integer.MIN_VALUE;
		 
		while (true) {
			System.out.println("Sisesta number (null lõpetab): ");
			int input = TextIO.getlnInt();
		
			
			if (input == 0) {
				break;
			}
			
			if (suurim < input) {
				suurim = input;
			}
			numbrid.add(input);
			
		}
		
		double kordaja = 10.0 / suurim;
		String[] iksid = new String[numbrid.size()]; //teen uue massiivi, et ümber keerata
		
		for (int i = 0; i < numbrid.size(); i++) {
			iksid[i] = genereeriIksid(numbrid.get(i), kordaja);
//			System.out.printf("%4d %s\n", numbrid.get(i), iksid); // %d - number, 2seal ees näitab, et 2 tähemärgi pikkune, %s - string
		}	
		System.out.println(Arrays.toString(iksid));
		
		for (int j = 9; j >= 0; j--) {
			for (int i = 0; i < iksid.length; i++) {
				if (iksid[i].length() > j) {
				System.out.printf("%2s ", iksid[i].charAt(j));
				}
			}
			System.out.println();
			
		}
		
		System.out.println("-------");
		for (int i = numbrid.size()-1; i >= 0; i--) {
			System.out.printf("%2d ", numbrid.get(i));
			
		}
		
	}

	private static String genereeriIksid(int nr, double kordaja) {
		String iksid = "";
		for (int i = 0; i < nr * kordaja; i++) {
			iksid = iksid + "x";
			
		}
		return iksid;
	}

}
