package projekt;


import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Component;
import java.awt.Cursor;
import javax.swing.JSpinner;
import javax.swing.JMenu;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseWheelListener;
import java.awt.event.MouseWheelEvent;
import javax.swing.JTextPane;

public class Brugerid extends JFrame {
	
	private static final long serialVersionUID = 1L;

	private JPanel contentPane;
	private JTextField txtMillistBurgeritSoovid;

	ArrayList<Toode> tooted = new ArrayList<Toode>();
	ArrayList<Double> summa = new ArrayList<Double>();
<<<<<<< HEAD
	private JTextField txtKoostis;
=======
>>>>>>> c27b1140473fb04a8a510103c622ec1bceacff65
	/**
	 * Launch the application.
	 */
	public static void BurgerJah() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Brugerid frame = new Brugerid();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Brugerid() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTextPane txtKoostis = new JTextPane();
		txtKoostis.setEditable(false);
		txtKoostis.setText("Hamburger - Meie ikooniks saanud burger koosneb 100% loomalihapihvist, sibulast, marineeritud kurgist ning just \u00F5igest kogusest sinepist ja ket\u0161upist v\u00E4rskelt r\u00F6stitud kukli vahel.");
		txtKoostis.setBounds(0, 11, 166, 135);
		contentPane.add(txtKoostis);
		txtKoostis.setVisible(false);
		
		txtMillistBurgeritSoovid = new JTextField();
		txtMillistBurgeritSoovid.setEditable(false);
		txtMillistBurgeritSoovid.setText("Millist burgerit soovid?");
		txtMillistBurgeritSoovid.setBounds(131, 11, 144, 20);
		contentPane.add(txtMillistBurgeritSoovid);
		txtMillistBurgeritSoovid.setColumns(10);
		
		JLabel lblHamburger = new JLabel("Hamburger");


		lblHamburger.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				txtKoostis.setVisible(true);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				txtKoostis.setVisible(false);
			}
		});
		lblHamburger.setIcon(new ImageIcon(Brugerid.class.getResource("/projekt/thumbs_hamburger.png")));
		lblHamburger.setBounds(-21, 86, 191, 190);
		contentPane.add(lblHamburger);
		
		JLabel lblKanaburger = new JLabel("Kanaburger");
		lblKanaburger.setIcon(new ImageIcon(Brugerid.class.getResource("/projekt/thumbs_chick_n_mccheese_v2.png")));
		lblKanaburger.setBounds(139, 103, 184, 173);
		contentPane.add(lblKanaburger);
		
		JLabel lblJuustuburger = new JLabel("Juustuburger");
		lblJuustuburger.setIcon(new ImageIcon(Brugerid.class.getResource("/projekt/thumbs_cheeseburger.png")));
		lblJuustuburger.setBounds(300, 95, 184, 173);
		contentPane.add(lblJuustuburger);
		
		JButton btnHamburger = new JButton("Hamburger 1.00\u20AC");
		btnHamburger.setBounds(10, 281, 139, 54);
		contentPane.add(btnHamburger);
		btnHamburger.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent evt) {		
            String command = evt.getActionCommand();
            if (command.equals("Hamburger 1.00\u20AC")) {
                Brugerid.BurgerJah();
        		
				tooted.add(new Toode("Hamburger", 1));
				summa.add(1.00);
				
				System.out.println("\nTeie tellimus: ");
				for (int i = 0; i < tooted.size(); i++) {
					// Character.toUpperCase;
					System.out.format("%s tk %.3g\u20AC", tooted.get(i), summa.get(i)); // http://stackoverflow.com/questions/153724/how-to-round-a-number-to-n-decimal-places-in-java
																					// joonamise
																					// viide

					// align.addColumn();
					System.out.println();
				}
				double arveSumma = 0;
				for (double i : summa) {
					arveSumma += i; // liidab summa ArrayListis
									// elemendid kokku
				}
				System.out.println("\nArve kokku: " + arveSumma + "\u20AC ja arve list: " + summa);
              
                }		}});
		
		

		
		JButton btnKanaburger = new JButton("Kanaburger 1.20\u20AC");
		btnKanaburger.setBounds(157, 281, 154, 54);
		contentPane.add(btnKanaburger);
		
		JButton btnJuustuburger = new JButton("Juustuburger 1.50\u20AC");
		btnJuustuburger.setBounds(321, 281, 154, 54);
		contentPane.add(btnJuustuburger);
		

	}
}
