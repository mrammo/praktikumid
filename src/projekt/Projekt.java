package projekt;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


import lib.TextIO;

public class Projekt extends JFrame {

	public static void main(String[] args) {

//		JFrame window = new JFrame("Restorani tellimus");

		System.out.println("Tere tulemast minu restorani! \nTellimuse vormistamiseks vajuta enterit!");
		Scanner keyboard = new Scanner(System.in); // http://codewithdesign.com/2010/01/17/create-a-press-enter-to-continue-with-java/
		keyboard.nextLine();

		ArrayList<Toode> tooted = new ArrayList<Toode>();
		ArrayList<Double> summa = new ArrayList<Double>();
		System.out.println("Kas soovid tellida burgerit? Vastuseks sisesta jah v�i ei.");

		while (true) {
			String burger = TextIO.getlnString();
			if (burger.equals("jah") || burger.equals("ja")) {
				// String juustuburger = "1";
				// String kanaburger = "2";
				// String hamburger = "3";

				BurgeritePildid2();
				System.out.println(
						"Millist burgerit soovid? Sisesta number \n 1. Juustuburger 1.50\u20AC \n 2. Kanaburger 1.20\u20AC \n 3. Hamburger 1.00\u20AC");
				int misBurger = TextIO.getlnInt();

				System.out.println("Sisesta toote kogus: ");
				int kogusBurger = TextIO.getlnInt();
				
				if (misBurger == 1) {
					tooted.add(new Toode("Juustuburger", kogusBurger));
					summa.add(kogusBurger * 1.50);
				} else if (misBurger == 2) {
					tooted.add(new Toode("Kanaburger", kogusBurger));
					summa.add(kogusBurger * 1.20);
				} else if (misBurger == 3) {
					tooted.add(new Toode("Hamburger", kogusBurger));
					summa.add(kogusBurger * 1.00);
				}
				System.out.println("Kas soovid veel m�nda burgerit? Vastuseks sisesta jah v�i ei.");

			} else {
				System.out.println("Aga friikartuleid? Vastuseks sisesta jah v�i ei.");
				while (true) {
					String friikartulid = TextIO.getlnString();

					if (friikartulid.equals("jah") || friikartulid.equals("ja")) {

						System.out.println(
								"Kui suuri friikartuleid soovid? Sisesta number \n 1. V�iksed 1.00\u20AC \n 2. Keskmised 1.20\u20AC \n 3. Suured 1.40\u20AC");
						int misFriikartulid = TextIO.getlnInt();
						System.out.println("Sisesta kogus: ");
						int kogusFriikartulid = TextIO.getlnInt();
						if (misFriikartulid == 1) {
							tooted.add(new Toode("V�iksed friikartulid", kogusFriikartulid));
							summa.add(kogusFriikartulid * 1.00);
						} else if (misFriikartulid == 2) {
							tooted.add(new Toode("Keskmised friikartulid", kogusFriikartulid));
							summa.add(kogusFriikartulid * 1.20);
						} else if (misFriikartulid == 3) {
							tooted.add(new Toode("Suured friikartulid", kogusFriikartulid));
							summa.add(kogusFriikartulid * 1.40);
						}
						System.out.println("Kas soovid veel friikartuleid? Vastuseks sisesta jah v�i ei.");
					} else {
						System.out.println("Aga juua? Vastuseks sisesta jah v�i ei.");
						while (true) {
							String jook = TextIO.getlnString();
							// arve osa:
							System.out.println(tooted);
							System.out.println("\nTeie tellimus: ");
							for (int i = 0; i < tooted.size(); i++) {
								// Character.toUpperCase;
								System.out.format("%s tk %.3g\u20AC", tooted.get(i), summa.get(i)); // http://stackoverflow.com/questions/153724/how-to-round-a-number-to-n-decimal-places-in-java
																								// joonamise
																								// viide

								// align.addColumn();
								System.out.println();
							}
							double arveSumma = 0;
							for (double i : summa) {
								arveSumma += i; // liidab summa ArrayListis
												// elemendid kokku
							}
							System.out.println("\nArve kokku: " + arveSumma + "\u20AC ja arve list: " + summa);
						}

					}
				}
			}

		}

		// piltide kuvamine

	}



	public static void BurgeritePildid2() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BurgeritePildid frame = new BurgeritePildid();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		});

	}
}
