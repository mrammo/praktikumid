package projekt;

public class Toode {	
	
	String nimi;
	int kogus;
	
	public Toode(String nimi, int kogus) {
		this.nimi = nimi;
		this.kogus = kogus;
	}

@Override
public String toString() {
	return nimi + " " + kogus;
}
}