package praktikum11;

public class Astenda {

	public static void main(String[] args) {
		System.out.println(astenda(2,4));

	}

	private static int astenda(int i, int j) {
		if (j==1) {
			return i;	
		}
		return i*astenda(i,j-1);
	
	}
}
