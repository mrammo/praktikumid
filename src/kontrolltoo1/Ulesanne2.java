package kontrolltoo1;

public class Ulesanne2 {

	   public static void main (String[] args) {
		      System.out.println (allaKeskmise (new double[]{1.0, 2.0, 3.0, 0.0, 0.0, 1.0, 4.0, 5.0, 2.0, 0.0}));
		      // YOUR TESTS HERE
		   }

		   public static int allaKeskmise (double[] d) {
			   float summa = 0;
			   for (int i = 0; i < d.length; i++) {
				   summa += d[i];   
			   }

			   float aritmeetilineKeskmine = summa / d.length;

			   int elementideArv = 0;
			   for (int i = 0; i < d.length; i++) {
				   if (d[i] < aritmeetilineKeskmine) {
					   elementideArv += 1;	
				   } 	
			   }
//			   System.out.println(summa);
//			   System.out.println(d.length);
//			  System.out.println(aritmeetilineKeskmine);
		      return elementideArv;	
		      }

}
