package lumememmeMang;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.canvas.GraphicsContext;

/**
 * Klass LumeSprite sisaldab muutujaid ja meetodeid, mida on vaja piltidega
 * (lumememme, lumehelbe ja j��purikaga) seotud toimingute jaoks.
 * 
 * @author Marite
 */
public class LumeSprite {
	Image pilt;
	double koordinaatX;
	double koordinaatY;
	double laius;
	double korgus;
	double kiirus = 3;
	double resetY = 500;

	/**
	 * Meetod pildi loomiseks failist lugemise kaudu. Lisaks m��rab meetod pildi
	 * laiuse funktsiooni getWidth() ja k�rguse funktsiooni getHeight() abil.
	 * Viide: Pildi loomine on Lee Stemkoski �petusest.
	 * 
	 * @param failinimi
	 *            pildi faili asukoht
	 */
	public void looPilt(String failinimi) {
		Image i = new Image(failinimi);
		ImageView imageView = new ImageView();
		imageView.setImage(i);
		pilt = i;
		laius = i.getWidth();
		korgus = i.getHeight();
	}

	/**
	 * Meetod m��rab pildi alguskoordinaadid. Meie programmis kasutan meetodit
	 * lumememme alguskoordinaatide m��ramiseks.
	 * 
	 * @param x
	 *            pildi X koordinaat (pildi vasaku �lemise nurga oma)
	 * @param y
	 *            pildi Y koordinaat (pildi vasaku �lemise nurga oma)
	 */
	public void koordinaadid(double x, double y) {
		koordinaatX = x;
		koordinaatY = y;
	}

	/**
	 * Meetod on lumememme liikumise jaoks. Kasutab liikumiseks sammu pikkust ja
	 * m��rab lumememme X koordinaadi. Lisaks kasutab meetodit kontrolliSeina,
	 * mis ei lase lumememmel paremale ja vasakule seinast nii-�elda l�bi minna.
	 * 
	 * @param samm
	 *            liikumisel �he sammu pikkus
	 */
	public void liikumine(int samm) {
		koordinaatX += samm;
		this.kontrolliSeina();
	}

	/**
	 * Meetod kontrollib, kas �ks pilt kattub teise pildiga (meie programmi
	 * puhul kas lumememm kattub lumehelbega/j��purikaga).
	 * 
	 * @param pilt
	 * @return true v�i false. Kui pildid kattuvad, siis v�ljastab t�ese
	 *         vastuse.
	 */
	public boolean kasKattub(LumeSprite pilt) {
		if (this.servaKoordinaadidVasak() < pilt.servaKoordinaadidParem()
				&& this.servaKoordinaadidParem() > pilt.servaKoordinaadidVasak()
				&& this.servaKoordinaadidAll() > pilt.servaKoordinaadidUleval()
				&& this.servaKoordinaadidUleval() < pilt.servaKoordinaadidAll()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Meetod reset m��rab pildi algkoordinaadid. KoordinaatX puhul on kasutatud
	 * matemaatilist funktsiooni random, mis juhuslikul teel valib pildi X
	 * koordinaadi algasukoha, kust lumehelves/j��purikas hakkab alla langema. Y
	 * koordinaat on alati 0, sest pilt hakkab langema �lemisest ��rest.
	 */
	public void reset() {
		koordinaatX = Math.random() * (500 - laius);
		koordinaatY = 0;
	}

	/**
	 * Meetod kontrollib, kas pildi (lumehelbe v�i j��purika) Y koordinaat on
	 * suurem kui meie poolt m��ratud resetY (lumehelbe puhul 500, j��purika
	 * puhul 2000) ja kui on, siis t�stab pildi tagasi �les, kust hakkab pilt
	 * j�lle otsast peale alla tulema.
	 */
	public void kontrolli() {
		if (koordinaatY > resetY) {
			this.reset();
		}
	}

	/**
	 * Meetod ei lase lumememmel akna piiridest v�lja minna. Kui lumememme X
	 * koordinaat on 0, siis l�kkab meetod lumememme koordinaadi 1 peale ja see
	 * peatab lumememme liikumise vasakule poole. Vastavalt ka paremale poole
	 * liikumise puhul.
	 */
	public void kontrolliSeina() {
		if (koordinaatX <= 0) {
			koordinaatX = 1;
		} else if (koordinaatX >= (500 - laius)) {
			koordinaatX = 500 - laius - 1;
		}
	}

	/**
	 * Meetod on lumehelbe ja j��purika liikumise jaoks, arvutab pildi uue Y
	 * koordinaadi kasutades muutujat kiirus. Lisaks kasutab meetod teist
	 * meetodit kontrolli.
	 */
	public void liikumine() {
		koordinaatY += kiirus;
		this.kontrolli();
	}

	/**
	 * Meetod tagastab pildi vasakpoolse serva koordinaadi, milleks on (vasaku
	 * �lemise nurga) X koordinaat.
	 * 
	 * @return pildi vasakpoolse serva koordinaat
	 */
	public double servaKoordinaadidVasak() {
		return koordinaatX;
	}

	/**
	 * Meetod tagastab pildi parempoolse serva koordinaadi, milleks on (vasaku
	 * �lemise nurga) X koordinaat + pildi laius.
	 * 
	 * @return pildi parempoolse serva koordinaat
	 */
	public double servaKoordinaadidParem() {
		return koordinaatX + laius;
	}

	/**
	 * Meetod tagastab pildi alumise serva koordinaadi, milleks on (vasaku
	 * �lemise nurga) Y koordinaat + pildi k�rgus.
	 * 
	 * @return pildi alumise serva koordinaat
	 */
	public double servaKoordinaadidAll() {
		return koordinaatY + korgus;
	}

	/**
	 * Meetod tagastab pildi �lemise serva koordinaadi, milleks on (vasaku
	 * �lemise nurga) Y koordinaat.
	 * 
	 * @return pildi �lemise serva koordinaat
	 */
	public double servaKoordinaadidUleval() {
		return koordinaatY;
	}

	/**
	 * Meetod render joonistab pildi canvasele (uude kohta) kasutades
	 * GraphicsContexti meetodit drawImage, mille parameetrid on pilt ja pildi
	 * koordinaadid (x ja y).
	 * Viide: Lee Stemkoski �petusest.
	 * 
	 * @param gc
	 */
	public void render(GraphicsContext gc) {
		gc.drawImage(pilt, koordinaatX, koordinaatY);
	}

	/**
	 * Meetod esitab objekti loetavas vormis.
	 * Viide: Lee Stemkoski �petusest.
	 */
	public String toString() {
		return "Koordinaadid(" + koordinaatX + "," + koordinaatY + ")";
	}
}