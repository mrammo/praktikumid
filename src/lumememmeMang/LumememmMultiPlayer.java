package lumememmeMang;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class LumememmMultiPlayer extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override // @Override on m�rge, mis �tleb, et k�esolev meetod on super
				// klassist (Application) �le kirjutatud. Nimelt Application
				// klassis on meetod start() juba olemas, aga meie kirjutame
				// selle tegevused enda vajaduse j�rgi �le.
	public void start(Stage aken) {

		aken.setTitle("Lumememme m�ng"); // akna pealkiri
		aken.setResizable(false); // akna suurus ei ole muudetav

		StackPane mang = new StackPane();
		Scene stseen = new Scene(mang, 500, 500);
		stseen.setFill(Paint.valueOf("#EFF5FB"));
		aken.setScene(stseen);

		Canvas canvas = new Canvas(500, 500);
		GraphicsContext gc = canvas.getGraphicsContext2D();
		mang.getChildren().add(canvas);

		ManguAndmed ManguAndmed = new ManguAndmed();
		ManguAndmed.skoor = 0;
		ManguAndmed.skoorLabi = 3;
		ManguAndmed.skoorAlgus = 0;
		ManguAndmed.suund = 0;
		ManguAndmed.elud = 2;
		ManguAndmed.eludLabi = 0;
		ManguAndmed.eludAlgus = 2;

		ManguAndmed.skoor2 = 0;
		ManguAndmed.suund2 = 0;
		ManguAndmed.elud2 = 2;

		LumeSprite lumememm1 = new LumeSprite();
		lumememm1.looPilt("file:src/lumememmeMang/lumememm.gif");
		lumememm1.koordinaadid(320, 400);

		LumeSprite lumememm2 = new LumeSprite();
		lumememm2.looPilt("file:src/lumememmeMang/lumememm2.gif");
		lumememm2.koordinaadid(120, 400);

		LumeSprite lumehelves = new LumeSprite();
		lumehelves.looPilt("file:src/lumememmeMang/lumehelves.gif");
		lumehelves.reset();

		LumeSprite purikas = new LumeSprite();
		purikas.looPilt("file:src/lumememmeMang/purikas.gif");
		purikas.reset();
		purikas.kiirus = 5;
		purikas.resetY = 2000;

		stseen.setOnKeyPressed(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent keyEvent) {

				if (keyEvent.getCode() == KeyCode.RIGHT) {
					ManguAndmed.suund = 1;
				} else if (keyEvent.getCode() == KeyCode.LEFT) {
					ManguAndmed.suund = -1;
				} else if (keyEvent.getCode() == KeyCode.S) {
					ManguAndmed.suund2 = 1;
				} else if (keyEvent.getCode() == KeyCode.A) {
					ManguAndmed.suund2 = -1;
				} else if (keyEvent.getCode() == KeyCode.SPACE) {
					ManguAndmed.alusta = 1;
				}
			}
		});

		stseen.setOnKeyReleased(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent keyEvent) {

				if (keyEvent.getCode() == KeyCode.RIGHT || keyEvent.getCode() == KeyCode.LEFT) {
					ManguAndmed.suund = 0;
				} else if (keyEvent.getCode() == KeyCode.A || keyEvent.getCode() == KeyCode.S) {
					ManguAndmed.suund2 = 0;
				} else if (keyEvent.getCode() == KeyCode.SPACE) {
					ManguAndmed.alusta = 0;
				}
			}
		});

		new AnimationTimer() {

			@Override
			public void handle(long now) {

				if (ManguAndmed.skoor == ManguAndmed.skoorLabi || ManguAndmed.elud == ManguAndmed.eludLabi
						|| ManguAndmed.skoor2 == ManguAndmed.skoorLabi || ManguAndmed.elud2 == ManguAndmed.eludLabi) {

					mang.getChildren().clear();

					if (ManguAndmed.elud == ManguAndmed.eludLabi && ManguAndmed.elud2 == ManguAndmed.eludLabi) {
						alusta("M\u00E4ng l\u00E4bi! Viik! M�lemal lumememmel elud otsas!");
					} else if (ManguAndmed.skoor == ManguAndmed.skoorLabi
							&& ManguAndmed.skoor2 == ManguAndmed.skoorLabi) {
						alusta("Palju �nne! Viik! M�lemal lumememmel punktid koos!");
					} else if (ManguAndmed.elud == ManguAndmed.eludLabi) {
						alusta("M\u00E4ng l\u00E4bi! \nPUNANE V�ITIS! Rohelisel lumememmel elud otsas!");
					} else if (ManguAndmed.skoor == ManguAndmed.skoorLabi) {
						alusta("Palju �nne! Roheline lumememm v�itis!");
					} else if (ManguAndmed.elud2 == ManguAndmed.eludLabi) {
						alusta("M\u00E4ng l\u00E4bi! \nROHELINE V�ITIS! Punasel lumememmel elud otsas!");
					} else if (ManguAndmed.skoor2 == ManguAndmed.skoorLabi) {
						alusta("Palju �nne! Punane lumememm v�itis!");
					}

				} else {
					gc.clearRect(0, 0, 500, 500); // kustutab eelmise pildi �ra
					lumememm1.liikumine(ManguAndmed.suund * 2);
					lumememm1.render(gc);
					lumememm2.liikumine(ManguAndmed.suund2 * 2);
					lumememm2.render(gc);
					lumehelves.liikumine();
					lumehelves.render(gc);
					purikas.liikumine();
					purikas.render(gc);

					if (lumememm1.kasKattub(lumehelves) && lumememm2.kasKattub(lumehelves)) {
						lumehelves.reset();
						ManguAndmed.skoor++;
						ManguAndmed.skoor2++;
					}

					if (lumememm1.kasKattub(purikas) && lumememm2.kasKattub(purikas)) {
						lumehelves.reset();
						ManguAndmed.elud--;
						ManguAndmed.elud2--;
					}

					if (lumememm1.kasKattub(lumehelves)) {
						lumehelves.reset();
						ManguAndmed.skoor++;
					}

					if (lumememm1.kasKattub(purikas)) {
						purikas.reset();
						ManguAndmed.elud--;
					}

					if (lumememm2.kasKattub(lumehelves)) {
						lumehelves.reset();
						ManguAndmed.skoor2++;
					}

					if (lumememm2.kasKattub(purikas)) {
						purikas.reset();
						ManguAndmed.elud2--;
					}

					String punktid = "Punkte: " + ManguAndmed.skoor + "\nElusid: " + ManguAndmed.elud;
					String punktid2 = "Punkte: " + ManguAndmed.skoor2 + "\nElusid: " + ManguAndmed.elud2;

					gc.strokeText(punktid, 400, 30);
					gc.strokeText(punktid2, 40, 30);
				}
			}

			private void alusta(String tekst) {
				Text t = new Text(tekst + "\nUue m\u00E4ngu alustamiseks vajuta t\u00FChikut!");
				mang.getChildren().add(t);
				if (ManguAndmed.alusta == 1) {
					mang.getChildren().remove(t);
					lumememm1.koordinaadid(320, 400);
					lumememm2.koordinaadid(120, 400);
					lumehelves.reset();
					purikas.reset();
					ManguAndmed.elud = ManguAndmed.eludAlgus;
					ManguAndmed.skoor = ManguAndmed.skoorAlgus;
					ManguAndmed.elud2 = ManguAndmed.eludAlgus;
					ManguAndmed.skoor2 = ManguAndmed.skoorAlgus;
					mang.getChildren().add(canvas);
				}

			}
		}.start();

		aken.show();

	}

}
