package praktikum9;

import lib.TextIO;
import praktikum7.TagurpidiSona;

public class Palindroom {

	public static void main(String[] args) {
		
		System.out.println("Palun sisesta sõna:");
		String s6na = TextIO.getlnString();
		
		if (onPalindroom(s6na)) {
			System.out.println("On palindroom");
		} else {
			System.out.println("Tegemist ei ole palindroomiga.");
		}

	}

	private static boolean onPalindroom(String s6na) {
		return s6na.equals(TagurpidiSona.tagurpidi(s6na));
	}

}
