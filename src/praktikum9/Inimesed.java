package praktikum9;

import java.util.ArrayList;

import lib.TextIO;

public class Inimesed {

	public static void main(String[] args) {
		ArrayList<Inimene> inimesed2 = new ArrayList<Inimene>();

		while (true) {
			
			System.out.println("Sisesta inimese nimi: ");
			String nimi = TextIO.getlnString();
			if (nimi.equals("")) {
				break;
			}
			System.out.println("Sisesta vanus: ");
			int vanus = TextIO.getlnInt();
			inimesed2.add(new Inimene(nimi, vanus));
			
		}
		for (Inimene inimene : inimesed2) {
			inimene.tervita();
		}
		System.out.println(inimesed2);

	}

}
