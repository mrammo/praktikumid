package praktikum5;

public class Tabel6 {

	public static void main(String[] args) {
		
		int tabeliSuurus = 10;

		for (int i = 0; i < tabeliSuurus; i++) {
			for (int j = 0; j < tabeliSuurus; j++) {
				if (i + j < 10) {
					System.out.print(i + j + " ");
				} else {
					System.out.print(i + j - 10 + " ");
				}
				//System.out.print("(i:" + i + " ;j:" + j + ") ");
			}
			System.out.println();
		}

	}

}
