package praktikum5;

public class Tsyklid {

	public static void main(String[] args) {
//		
//		if (true) {
//			System.out.println("tingimus on tõene");
//		}
		
//		int arv = 0;
//		while (arv < 3) {
//			System.out.println("tingimus on tõene, arv: " + arv);
//			arv = arv + 1;
//			//continue; - läheb tsükli algusesse
//			//break; //katkestab tsükli töö
			//kui me ei tea, mitu korda tahame tsüklit korrata, siis while on hea
//		}
		
		for (int i = 0; i < 10; i = i + 1 /*samaväärne on i++*/) {
			System.out.println(i);
			
		}

	}

}
