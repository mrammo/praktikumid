package praktikum8;

import praktikum6.Meetodid;

public class Kuulujutugeneraator {

	public static void main(String[] args) {
	
		
		String[] naised = {"Juta", "Mari", "Kati"};
		String[] mehed = {"Kaspar", "Martin", "Kristjan"};
		String[] tegusonad = {"jooksevad", "laulavad", "tantsivad"};
		
		System.out.format("%s ja %s %s.", suvalineElement(naised), suvalineElement(mehed), suvalineElement(tegusonad));
	
//		String naiseNimi = suvalineElement(naised);
		
//		System.out.println(suvalineElement(tegusonad));
		
//		int suvalineNaine = (int)(Math.random()* naised.length);
//		System.out.println("Naistest valis: "+ naised[suvalineNaine]);
//		
//		int suvalineMees = (int)(Math.random()* mehed.length);
//		System.out.println("Meestest valis: "+ mehed[suvalineMees]);
//		
//		int suvalineTegusona = (int)(Math.random()* tegusonad.length);
//		System.out.println("Tegusõnadest valis: "+ tegusonad[suvalineTegusona]);
//
//		
//		System.out.println(naised[suvalineNaine] + " ja " + mehed[suvalineMees] + " " + tegusonad[suvalineTegusona] + ".");
	}

	public static String suvalineElement(String[] sonad) {
		int suvalineIndeks = Meetodid.suvalineArv(0, sonad.length - 1);
		return sonad[suvalineIndeks];
	}
	
	
}
