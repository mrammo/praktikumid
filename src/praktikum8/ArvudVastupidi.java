package praktikum8;

import lib.TextIO;

public class ArvudVastupidi {

	public static void main(String[] args) {
		

		
//		int[] veelArve = {23, 55, 66, 67};
//		System.out.println(arvud[1]);
//		
		
		//1. võimalus:
		
//		int[] arvud = new int[3];
		
//		for (int i = 0; i < arvud.length; i++) {
//			System.out.println("Kirjuta arv indeksiga " + i);
//			arvud[i] = TextIO.getlnInt();
//		}
//		
//		for (int i = arvud.length - 1; i >= 0; i--) {
//			System.out.println(arvud[i]);
//		}
//		

		//2. võimalus, stringiga lahendus
		
		String tagurpidi = "";
		for (int i = 0; i < 3; i++) {
			System.out.println("sisesta väärtus indeksile " + i);
			tagurpidi = TextIO.getlnInt() + "\n" + tagurpidi;
		}
		System.out.println(tagurpidi);

	}

}
