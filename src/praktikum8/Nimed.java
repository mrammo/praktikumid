package praktikum8;

import java.util.ArrayList;
import java.util.Collections;

import lib.TextIO;

public class Nimed {

	public static void main(String[] args) {
			
	
		ArrayList<String> nimed = new ArrayList<String>();
		
		System.out.println("Sisesta nimesid (tühi sisestus lõpetab): ");
		
		while (true){
			String nimi = TextIO.getlnString();
			
			if (nimi.equals("")) {
				break;
			}
			nimed.add(nimi);
			
		}
		
		Collections.sort(nimed);
		
		for (String nimi : nimed) {
			System.out.println(nimi);
			
		}
			
//		System.out.println(nimed);
		
//		nimed.add("Mati");
//		nimed.add("Kati");
//		
//		for (String nimi : nimed) { // foreach
//			System.out.println(nimi);
//		}
//		
		
		
	}

}
