package praktikum15;

public class Joon {

	Punkt algus, lõpp; 
	
	public Joon(Punkt p1,  Punkt p2) {
		algus = p1;
		lõpp = p2; 
	}
	
	public double pikkus() {		
		

		double pikkus; 
		pikkus = Math.sqrt(Math.pow((algus.x-lõpp.x), 2) + Math.pow((algus.y-lõpp.y), 2));
		
		return pikkus; 
		
		
	}
	
	@Override
	public String toString() {
		return "Joon(" + algus + "--" + lõpp + ")";
	}
	
}
