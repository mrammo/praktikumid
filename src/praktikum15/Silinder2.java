package praktikum15;

public class Silinder2 extends Ring {

	double kõrgus; 
	
	public Silinder2(Ring r, double h) {
		super(r.keskpunkt, r.raadius);
		kõrgus = h;
	}

	public double pindala() {
		return super.pindala() * 2 + super.ümbermõõt() * kõrgus;
	}
	
	public double ruumala() {		
		return super.pindala() * kõrgus;
	}
	
	@Override
	public String toString() {
		return "Silinder";
	}
	
}
