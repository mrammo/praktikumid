package praktikum15;

public class Ring {
	
	Punkt keskpunkt;
	double raadius; 
	
	public Ring(Punkt p1, double r) {
		keskpunkt = p1;
		raadius = r;
	}
	
	public double ümbermõõt() {		
		
		double ümbermõõt = Math.PI * 2 * raadius;
		
		return ümbermõõt; 
	}
	
	public double pindala() {		
		
		double pindala = Math.PI * Math.pow(raadius, 2);
		
		return pindala; 
	}
	
	@Override
	public String toString() {
		return "Ring(" + keskpunkt + ", " + raadius + ")";
	}
	
	
	
		

}
