package praktikum15;

public class Katsetused {

	public static void main(String[] args) {
		
		String tekst = new String("Tere");
		
		Punkt p1 = new Punkt(2, 4);
		Punkt p2 = new Punkt(3, 5);

		Joon j = new Joon(p1, p2);
		System.out.println(j);
		System.out.println(j.pikkus());
		
//		System.out.println(p1);
//		System.out.println(p2);
//		
		double raadius = 5;
		Ring r = new Ring(p1, raadius);
//		System.out.println(r.ümbermõõt());
//		System.out.println(r.pindala());
		
//		double s = r.pindala();
//		double p = r.ümbermõõt();
//		System.out.println(p);
		
//		double h = 10; 
//		Silinder silinder = new Silinder(s, p, h);
//		System.out.println(silinder.pindala);
//		System.out.println(silinder.ruumala());
		
		Silinder2 s = new Silinder2(r, 10);
		System.out.println("Silindri pindala " + s.pindala());
		System.out.println("Silindri ruumala " + s.ruumala());

	}

}
