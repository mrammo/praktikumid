package projekt2;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import projekt.Brugerid;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import java.awt.event.ActionListener;

public class Tellimus extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Tellimus frame = new Tellimus();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Tellimus() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnAlustaTellimust = new JButton("Alusta tellimust!");
		btnAlustaTellimust.setBounds(117, 155, 191, 57);
		contentPane.add(btnAlustaTellimust);
		btnAlustaTellimust.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				
	            String command = evt.getActionCommand();
	            if (command.equals("Alusta tellimust!")) {
	                projekt.Projekt2.TelliBurger();
	            }}}
		);

		
		JLabel lblTereTulemastMinu = new JLabel("Tere tulemast minu restorani!");
		lblTereTulemastMinu.setBounds(115, 48, 191, 80);
		contentPane.add(lblTereTulemastMinu);
		
		JLabel label = new JLabel("");
		label.setBounds(159, 128, 46, 14);
		contentPane.add(label);
		

	}}