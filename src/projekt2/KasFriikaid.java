package projekt2;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import projekt.Brugerid;

import javax.swing.JLabel;
import javax.swing.JButton;

public class KasFriikaid extends JFrame {
	
	private static final long serialVersionUID = 1L;

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					KasFriikaid frame = new KasFriikaid();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public static void FriikadJahEi() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					KasFriikaid frame = new KasFriikaid();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public KasFriikaid() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblKasSoovidTellida = new JLabel("Kas soovid tellida friikartuleid?");
		lblKasSoovidTellida.setBounds(27, 24, 356, 60);
		contentPane.add(lblKasSoovidTellida);
		
		JButton btnJah = new JButton("JAH");
		btnJah.setBounds(66, 143, 89, 23);
		contentPane.add(btnJah);
		
		JButton btnEi = new JButton("EI");
		btnEi.setBounds(222, 143, 89, 23);
		contentPane.add(btnEi);
		
		btnJah.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent evt) {
			
            String command = evt.getActionCommand();
            if (command.equals("JAH")) {
                projekt.Brugerid.BurgerJah();
              
                }}}
);}
	
	

}
