package lumememm;
//
//import javafx.animation.AnimationTimer;
//import javafx.application.Application;
//import javafx.event.EventHandler;
//import javafx.scene.Scene;
//import javafx.scene.canvas.Canvas;
//import javafx.scene.canvas.GraphicsContext;
//import javafx.scene.input.KeyCode;
//import javafx.scene.input.KeyEvent;
//import javafx.scene.layout.StackPane;
//import javafx.scene.paint.Paint;
//import javafx.scene.text.Text;
//import javafx.stage.Stage;
//import lumememmeMang.LumeSprite;
//import lumememmeMang.ManguAndmed;
//
///**
// * Klass LumememmeMang on p�hiklass m�ngu toimingute jaoks. Klass kasutab
// * Applicationit, millega saame JavaFX v�imalusi kasutada.
// * 
// * @author Marite
// *
// */
//public class Lumememm3 extends Application {
//
//	/**
//	 * Main meetod, mis paneb m�ngu t��le. Kutsub v�lja launch() meetodi, mis
//	 * kutsub v�lja init() meetodi, siis start() meetodi ja kui rakendus l�ppeb,
//	 * siis stop() meetodi. Nendest peame �mber kirjutama vaid start() meetodi.
//	 * 
//	 * @param args
//	 */
//	public static void main(String[] args) {
//		launch(args);
//	}
//
//	/**
//	 * start meetod, kus on rakenduse sisu, mis toimub, milline v�lja n�eb jne.
//	 */
//	@Override // @Override on m�rge, mis �tleb, et k�esolev meetod on super
//				// klassist (Application) �le kirjutatud. Nimelt Application
//				// klassis on meetod start() juba olemas, aga meie kirjutame
//				// selle tegevused enda vajaduse j�rgi �le.
//	public void start(Stage aken) {
//
//		aken.setTitle("Lumememme m�ng"); // Akna pealkiri
//		aken.setResizable(false); // Akna suurus ei ole muudetav
//
//		StackPane mang = new StackPane();
//		Scene stseen = new Scene(mang, 500, 500); // Suurus 500 x 500
//		stseen.setFill(Paint.valueOf("#EFF5FB")); // Tausta v�rviks m��rame
//													// helesinise
//		aken.setScene(stseen);
//
//		Canvas canvas = new Canvas(500, 500); // Canvas on objekt, millele saame
//												// joonistada teksti, kujundeid,
//												// pilte.
//		GraphicsContext gc = canvas.getGraphicsContext2D(); // Joonistamiseks kasutame GraphicContexti.
//		mang.getChildren().add(canvas); // Lisame loodud canvase Pane-le mang.
//
//		// you draw on the canvas using graphicscontext, canvas on nagu paber ja
//		// graphicscontext on nagu pliiats.
//		// http://docs.oracle.com/javase/8/javafx/api/javafx/scene/canvas/GraphicsContext.html
//
//		// lihtsalt int skoor = 0; ei saanud kasutada, sest tegemist static
//		// meetodiga, tuleb teha eraldi klass/v�i lihtsalt v�ljaspool sisemisi
//		// klasse eraldi defineerida (n�iteks enne main klassi) ja siis saab
//		// kasutada. Sisemise meetodi sees peab olema l�plik, aga l�pliku ei saa
//		// enam muuta. Java erip�ra
//		ManguAndmed ManguAndmed = new ManguAndmed(); // M��rame m�nguks vajalike muutujate v��rtused.
//		ManguAndmed.skoor = 0;
//		ManguAndmed.skoorLabi = 2;
//		ManguAndmed.skoorAlgus = 0;
//		ManguAndmed.suund = 0;
//		ManguAndmed.elud = 2;
//		ManguAndmed.eludLabi = 0;
//		ManguAndmed.eludAlgus = 2;
//
//		// Image lumememm = new Image("file:src/lumememm/lumememm.jpg");
//		// ImageView imageView = new ImageView();
//		// imageView.setImage(lumememm);
//		// manguOsa.getChildren().add(imageView); // kutsub pildi esile
//
//		LumeSprite lumememm = new LumeSprite(); // Loome uue LumeSprite'i lumememm, millel saame n��d kasutada LumeSprite klassis loodud meetodeid.
//		lumememm.looPilt("file:src/lumememmeMang/lumememm.gif"); // Loome lumememme pildi.
//		double alguskoordinaatX = (500 - lumememm.laius) / 2; // Et lumememm oleks keskel.
//		double alguskoordinaatY = 500 - lumememm.korgus; // Et lumememm oleks alumises ��res.
//		lumememm.koordinaadid(alguskoordinaatX, alguskoordinaatY); // M��rame lumememme alguskoordinaadid.
//
//		LumeSprite lumehelves = new LumeSprite(); // Loome uue LumeSprite'i lumehelves.
//		lumehelves.looPilt("file:src/lumememmeMang/lumehelves.gif");
//		lumehelves.reset(); // Kasutame meetodit reset, et lumehelves hakkaks ekraani �lemisest ��rest alla kukkuma.
//
//		LumeSprite purikas = new LumeSprite();
//		purikas.looPilt("file:src/lumememmeMang/purikas.gif");
//		purikas.reset();
//		purikas.kiirus = 5; // M��rame j��purika kiiruse suuremaks kui lumehelbe oma, et j��purikas tuleks ootamatult ja selle eest oleks raskem �ra minna.
//		purikas.resetY = 2000; // M��rame j��purika uuesti tulemise aja pikemaks (j��purikas l�heb tagasi �les, kui on j��purika Y koordinaat on 2000).
//
//		/**
//		 * Kasutame KeyEvent-i setOnKeyPressed, mis l�heb t��le, 
//		 * kui klaviatuuril vastav nupp on alla vajutatud.
//		 * Selle kaudu saame lumememme liikuma panna ja m�ngu l�pus uut m�ngu alustada.
//		 * Kasutame nupu alla vajutamise j��dvustamiseks muutujaid (suund ja alusta).
//		 */
//		stseen.setOnKeyPressed(new EventHandler<KeyEvent>() {
//
//			@Override
//			public void handle(KeyEvent keyEvent) {
//
//				if (keyEvent.getCode() == KeyCode.RIGHT) {
//					System.out.println("Lumememm paremale");
//					ManguAndmed.suund = 1;
//				} else if (keyEvent.getCode() == KeyCode.LEFT) {
//					System.out.println("Lumememm vasakule");
//					ManguAndmed.suund = -1;
//				} else if (keyEvent.getCode() == KeyCode.SPACE) {
//					System.out.println("Alusta m�ngu");
//					ManguAndmed.alusta = 1;
//				}
//				keyEvent.consume();
//			}
//		});
//
//		/**
//		 * Kasutame KeyEvent-i setOnKeyPressed, mis l�heb t��le, 
//		 * kui klaviatuuril vastavalt nupp lahti lasta.
//		 * Kui n�pp nupult �ra v�tta, siis muutujad l�hevad tagasi endiseks.
//		 */
//		stseen.setOnKeyReleased(new EventHandler<KeyEvent>() {
//
//			@Override
//			public void handle(KeyEvent keyEvent) {
//
//				if (keyEvent.getCode() == KeyCode.RIGHT || keyEvent.getCode() == KeyCode.LEFT) {
//					ManguAndmed.suund = 0;
//				} else if (keyEvent.getCode() == KeyCode.SPACE) {
//					ManguAndmed.alusta = 0;
//				}
//				keyEvent.consume();
//			}
//		});
//
//		/**
//		 * Kasutame JavaFX klassi AnimationTimer, et muuta m�ng liikuvaks. 
//		 * Uuendab kaadrit 60 korda sekundis. Klass kasutab meetodit handle().
//		 * Klassi l�pus kasutame meetodit start(), mis k�ivitab animatsiooni.
//		 */
//		new AnimationTimer() {
//
//			@Override
//			public void handle(long now) {
//				
//				/*
//				 * If: Kui on elusid alles v�i skoor ei ole t�is, siis m�ng k�ib. Else: Kui punktid koos/elud otsas, siis m�ng l�heb pausile ja tuleb ette tekst, et m�ng l�bi.
//				 */
//				if (ManguAndmed.skoor != ManguAndmed.skoorLabi && ManguAndmed.elud != ManguAndmed.eludLabi) {
//
//					gc.clearRect(0, 0, 500, 500); // Kustutab canvaselt eelmise pildi �ra.
//					lumememm.liikumine(ManguAndmed.suund * 2);  // Kui nupp RIGHT v�i LEFT on alla vajutatud, 
//																// toimub liikumine, sest siis on suund 1 v�i -1, 
//																// muul juhul 0 ja liikumist ei toimu.
//					lumememm.render(gc); // Joonistab lumememme canvasele.
//					lumehelves.liikumine();
//					lumehelves.render(gc);
//					purikas.liikumine();
//					purikas.render(gc);
//
//					// gc.drawImage(lumememm2, 50, 50);
//					// Kontrollime, kas lumememm on vastu lumehelvest, kui on,
//					// siis lumehelves "h�ppab" tagasi �les ja m�ngija saab
//					// punkti juurde.
//					if (lumememm.kasKattub(lumehelves)) {
//						lumehelves.reset();
//						System.out.println("1");
//						ManguAndmed.skoor++;
//					}
//					
//					// Kontrollime, kas lumememm on vastu j��purikat, kui on,
//					// siis j��purikas "h�ppab" tagasi �les ja m�ngija kaotab punkti.
//					if (lumememm.kasKattub(purikas)) {
//						purikas.reset();
//						System.out.println("-1");
//						ManguAndmed.elud--;
//					}
//						
//					// Kirjutame canvasele punktide ja elude arvu.
//					String punktid = "Punkte: " + ManguAndmed.skoor + "\nElusid: " + ManguAndmed.elud;
//
//					gc.strokeText(punktid, 390, 30); // Joonistab teksti canvasele.
//				} else {
//					mang.getChildren().clear(); // Eemaldab k�ik m�ngulaualt ehk v�tab canvase �ra.
//
//					if (ManguAndmed.elud == ManguAndmed.eludLabi) {
//						alusta("M\u00E4ng l\u00E4bi! Elud otsas!");
//					} else if (ManguAndmed.skoor == ManguAndmed.skoorLabi) {
//						alusta("Palju �nne! Kogusid " + ManguAndmed.skoorLabi + " punkti!");
//					}
//				}
//			}
//
//			// Meetod m�ngu l�pu ja uue m�ngu alustamiseks. Kui vajutada
//			// t�hikut, siis algab uus m�ng.
//			private void alusta(String tekst) {
//				System.out.println("M\u00E4ng l\u00E4bi!");
//				Text t = new Text(tekst + "\nUue m\u00E4ngu alustamiseks vajuta t\u00FChikut!");
//				mang.getChildren().add(t); // Lisab teksti m�ngulauale.
//				if (ManguAndmed.alusta == 1) {
//					mang.getChildren().remove(t); // Eemaldab teksti m�ngulaualt.
//					lumememm.koordinaadid(alguskoordinaatX, alguskoordinaatY);
//					lumehelves.reset();
//					purikas.reset();
//					ManguAndmed.elud = ManguAndmed.eludAlgus;
//					ManguAndmed.skoor = ManguAndmed.skoorAlgus;
//					mang.getChildren().add(canvas); // Lisab canvase tagasi ja m�ng saab j�llea alata.
//				}
//
//			}
//		}.start();
//
//		aken.show(); // Toob akna n�htavale.
//
//	}
//
//}
//
//// H�sti palju �petas mind Lee Stemkoski JavaFX m�ngude tegemise �petus. Kasutasin sealt ka m�ningaid ideid.
//// Link: https://gamedevelopment.tutsplus.com/tutorials/introduction-to-javafx-for-game-development--cms-23835
