package lumememm;

import java.awt.EventQueue;
import javax.swing.JFrame;

public class Liikumine extends JFrame {

    public Liikumine() {
        
        initUI();
    }
    
    private void initUI() {
        
        add(new Board());
        
        setSize(400, 300);
        setResizable(false);
        
        setTitle("Lumememm");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                
                Liikumine ex = new Liikumine();
                ex.setVisible(true);
            }
        });
    }
}