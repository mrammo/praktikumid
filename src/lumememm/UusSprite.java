package lumememm;

import javafx.scene.image.Image;

public class UusSprite {
    private Image pilt;
    private double positsioonX;
    private double positsioonY;    
    private double kiirusX;
    private double kiirusY;
    private double laius;
    private double korgus;
    
    public UusSprite()
    {
        positsioonX = 0;
        positsioonY = 0;    
        kiirusX = 0;
        kiirusY = 0;
    }

    public void setImage(Image i)
    {
        pilt = i;
        laius = i.getWidth();
        korgus = i.getHeight();
    }
}
