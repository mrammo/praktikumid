package lumememm;

import javafx.scene.image.Image;
import javafx.scene.canvas.GraphicsContext;
import javafx.geometry.Rectangle2D;



public class LumehelbeSprite
{
    private Image image;
    private double positionX;
    private double positionY;    
    private double width;
    private double height;

    public LumehelbeSprite()
    {
        positionX = 0;
        positionY = 0;    
    }

    public void setImage(Image i)
    {
        image = i;
        width = i.getWidth();
        height = i.getHeight();
    }

    public void setImage(String filename)
    {
        Image i = new Image(filename);
        setImage(i);
    }

    public void setPosition(double x, double y)
    {
        positionX = x;
        positionY = y;
    }


    public void move()
    {
        positionY += 3;
    }
    
    public void kontrolli()
    {
    	if (positionY > 500) {
    		positionY = 0;
    	}
    }
    
    public double servaKordinaadidVasak()
    {
    	return positionX;
    }
    
    public double servaKordinaadidParem()
    {
    	return positionX+width;
    }
    
    public double servaKordinaadidAll()
    {
    	return positionY + height;
    }

    // joonistab pildi uude kohta
    public void render(GraphicsContext gc)
    {
        gc.drawImage( image, positionX, positionY );
    }

    public Rectangle2D getBoundary()
    {
        return new Rectangle2D(positionX,positionY,width,height);
    }

    public boolean intersects(Sprite2 s)
    {
        return s.getBoundary().intersects( this.getBoundary() );
    }
    
    public String toString()
    {
        return " Position: [" + positionX + "," + positionY + "]";
    }
}