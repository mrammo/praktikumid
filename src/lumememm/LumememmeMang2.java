//package lumememm;
//
//import javafx.animation.AnimationTimer;
//import javafx.application.Application;
//import javafx.event.EventHandler;
//import javafx.scene.Scene;
//import javafx.scene.canvas.Canvas;
//import javafx.scene.canvas.GraphicsContext;
//import javafx.scene.input.KeyCode;
//import javafx.scene.input.KeyEvent;
//import javafx.scene.layout.StackPane;
//import javafx.scene.paint.Paint;
//import javafx.scene.text.Text;
//import javafx.stage.Stage;
//import lumememmeMang.LumeSprite;
//import lumememmeMang.ManguAndmed;
//
///**
// * Klass LumememmeMang on p�hiklass m�ngu toimingute jaoks. Klass kasutab
// * Applicationit, millega saame JavaFX v�imalusi kasutada.
// * 
// * @author Marite
// *
// */
//public class LumememmeMang2 extends Application {
//
//	/**
//	 * Main meetod, mis paneb m�ngu t��le. Kutsub v�lja launch() meetodi, mis
//	 * kutsub v�lja init() meetodi, siis start() meetodi ja kui rakendus l�ppeb,
//	 * siis stop() meetodi. Nendest peame �mber kirjutama vaid start() meetodi.
//	 */
//	public static void main(String[] args) {
//		launch(args);
//	}
//
//	/**
//	 * start meetod, kus on rakenduse sisu, mis toimub, milline v�lja n�eb jne.
//	 */
//	@Override
//	public void start(Stage aken) {
//
//		aken.setTitle("Lumememme m�ng");
//		aken.setResizable(false); // Akna suurus ei ole muudetav
//
//		StackPane mang = new StackPane();
//		Scene stseen = new Scene(mang, 500, 500);
//		stseen.setFill(Paint.valueOf("#EFF5FB"));
//		aken.setScene(stseen);
//
//		Canvas canvas = new Canvas(500, 500);
//		GraphicsContext gc = canvas.getGraphicsContext2D();
//		mang.getChildren().add(canvas);
//
//		ManguAndmed ManguAndmed = new ManguAndmed();
//		ManguAndmed.skoor = 0;
//		ManguAndmed.skoorLabi = 2;
//		ManguAndmed.skoorAlgus = 0;
//		ManguAndmed.suund = 0;
//		ManguAndmed.elud = 2;
//		ManguAndmed.eludLabi = 0;
//		ManguAndmed.eludAlgus = 2;
//
//		LumeSprite lumememm = new LumeSprite();
//		lumememm.looPilt("file:src/lumememmeMang/lumememm.gif");
//		double alguskoordinaatX = (500 - lumememm.laius) / 2;
//		double alguskoordinaatY = 500 - lumememm.korgus;
//		lumememm.koordinaadid(alguskoordinaatX, alguskoordinaatY);
//
//		LumeSprite lumehelves = new LumeSprite();
//		lumehelves.looPilt("file:src/lumememmeMang/lumehelves.gif");
//		lumehelves.reset();
//
//		LumeSprite purikas = new LumeSprite();
//		purikas.looPilt("file:src/lumememmeMang/purikas.gif");
//		purikas.reset();
//		purikas.kiirus = 5;
//		purikas.resetY = 2000;
//
//		/**
//		 * Kasutame KeyEvent-i setOnKeyPressed, mis l�heb t��le, kui
//		 * klaviatuuril vastav nupp on alla vajutatud. Selle kaudu saame
//		 * lumememme liikuma panna ja m�ngu l�pus uut m�ngu alustada. Kasutame
//		 * nupu alla vajutamise j��dvustamiseks muutujaid (suund ja alusta).
//		 */
//		stseen.setOnKeyPressed(new EventHandler<KeyEvent>() {
//
//			@Override
//			public void handle(KeyEvent keyEvent) {
//
//				if (keyEvent.getCode() == KeyCode.RIGHT) {
//					ManguAndmed.suund = 1;
//				} else if (keyEvent.getCode() == KeyCode.LEFT) {
//					ManguAndmed.suund = -1;
//				} else if (keyEvent.getCode() == KeyCode.SPACE) {
//					ManguAndmed.alusta = 1;
//				}
//			}
//		});
//
//		/**
//		 * Kasutame KeyEvent-i setOnKeyPressed, mis l�heb t��le, kui
//		 * klaviatuuril vastav nupp lahti lasta. Kui n�pp nupult �ra v�tta,
//		 * siis muutujad l�hevad tagasi endiseks.
//		 */
//		stseen.setOnKeyReleased(new EventHandler<KeyEvent>() {
//
//			@Override
//			public void handle(KeyEvent keyEvent) {
//
//				if (keyEvent.getCode() == KeyCode.RIGHT || keyEvent.getCode() == KeyCode.LEFT) {
//					ManguAndmed.suund = 0;
//				} else if (keyEvent.getCode() == KeyCode.SPACE) {
//					ManguAndmed.alusta = 0;
//				}
//			}
//		});
//
//		/**
//		 * Kasutame JavaFX klassi AnimationTimer, et muuta m�ng liikuvaks.
//		 * Uuendab kaadrit 60 korda sekundis. Klass kasutab meetodit handle().
//		 * Klassi l�pus kasutame meetodit start(), mis k�ivitab animatsiooni.
//		 */
//		new AnimationTimer() {
//
//			@Override
//			public void handle(long now) {
//
//				// Kui punktid koos/elud otsas, siis m�ng l�heb pausile ja tuleb
//				// ette tekst, et m�ng l�bi. Muul juhul m�ng k�ib.
//				if (ManguAndmed.skoor == ManguAndmed.skoorLabi || ManguAndmed.elud == ManguAndmed.eludLabi) {
//
//					mang.getChildren().clear(); // Eemaldab k�ik m�ngulaualt ehk
//												// v�tab canvase �ra.
//
//					if (ManguAndmed.elud == ManguAndmed.eludLabi) {
//						alusta("M\u00E4ng l\u00E4bi! Elud otsas!");
//					} else if (ManguAndmed.skoor == ManguAndmed.skoorLabi) {
//						alusta("Palju �nne! Kogusid " + ManguAndmed.skoorLabi + " punkti!");
//					}
//				} else {
//
//					gc.clearRect(0, 0, 500, 500);
//					lumememm.liikumine(ManguAndmed.suund * 2);
//					lumememm.render(gc);
//					lumehelves.liikumine();
//					lumehelves.render(gc);
//					purikas.liikumine();
//					purikas.render(gc);
//
//					// Kontrollime, kas lumememm on vastu lumehelvest, kui on,
//					// siis lumehelves "h�ppab" tagasi �les ja m�ngija saab
//					// punkti juurde.
//					if (lumememm.kasKattub(lumehelves)) {
//						lumehelves.reset();
//						ManguAndmed.skoor++;
//					}
//
//					// Kontrollime, kas lumememm on vastu j��purikat, kui on,
//					// siis j��purikas "h�ppab" tagasi �les ja m�ngija kaotab
//					// punkti.
//					if (lumememm.kasKattub(purikas)) {
//						purikas.reset();
//						ManguAndmed.elud--;
//					}
//
//					String punktid = "Punkte: " + ManguAndmed.skoor + "\nElusid: " + ManguAndmed.elud;
//
//					gc.strokeText(punktid, 390, 30);
//				}
//			}
//
//			/**
//			 * Meetod m�ngu l�pu ja uue m�ngu alustamiseks. Kui vajutada
//			 * t�hikut, siis algab uus m�ng.
//			 * 
//			 * @param tekst
//			 *            tekst, mis v�ljastatakse vastavalt sellele, kas m�ng
//			 *            kaotati v�i v�ideti.
//			 */
//			private void alusta(String tekst) {
//				Text t = new Text(tekst + "\nUue m\u00E4ngu alustamiseks vajuta t\u00FChikut!");
//				mang.getChildren().add(t); // Lisab teksti m�ngulauale.
//				if (ManguAndmed.alusta == 1) {
//					mang.getChildren().remove(t);
//					lumememm.koordinaadid(alguskoordinaatX, alguskoordinaatY);
//					lumehelves.reset();
//					purikas.reset();
//					ManguAndmed.elud = ManguAndmed.eludAlgus;
//					ManguAndmed.skoor = ManguAndmed.skoorAlgus;
//					mang.getChildren().add(canvas);
//				}
//
//			}
//		}.start();
//
//		aken.show(); // Toob akna n�htavale.
//
//	}
//
//}
//
//// H�sti palju �petas mind Lee Stemkoski JavaFX m�ngude tegemise �petus.
//// Kasutasin sealt ka m�ningaid ideid.
//// Link:
//// https://gamedevelopment.tutsplus.com/tutorials/introduction-to-javafx-for-game-development--cms-23835
