package lumememm;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.Group;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.animation.AnimationTimer;
import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;
import java.util.ArrayList;
import java.util.Iterator;


public class Naide2 extends Application 
{
    public static void main(String[] args) 
    {
        launch(args);
    }

    @Override
    public void start(Stage aken) 
    {
        aken.setTitle( "Lumememme m�ng" );

        Group root = new Group();
        Scene stseen = new Scene( root );
        aken.setScene( stseen );
        stseen.setFill(Paint.valueOf("#EFF5FB"));

        Canvas canvas = new Canvas( 512, 512 );
        root.getChildren().add( canvas );

        ArrayList<String> sisend = new ArrayList<String>();

        stseen.setOnKeyPressed(
            new EventHandler<KeyEvent>()
            {
                public void handle(KeyEvent e)
                {
                    String kood = e.getCode().toString();
                    if ( !sisend.contains(kood) )
                        sisend.add( kood );
                }
            });

        stseen.setOnKeyReleased(
            new EventHandler<KeyEvent>()
            {
                public void handle(KeyEvent e)
                {
                    String kood = e.getCode().toString();
                    sisend.remove( kood );
                }
            });

        GraphicsContext gc = canvas.getGraphicsContext2D();

        Font theFont = Font.font( "Helvetica", FontWeight.BOLD, 24 );
        gc.setFont( theFont );
        gc.setFill( Color.GREEN );
        gc.setStroke( Color.BLACK );
        gc.setLineWidth(1);
        
        Sprite2 lumememm = new Sprite2();
        lumememm.setImage("file:src/lumememm/lumememm.jpg");
        lumememm.setPosition(200, 410);
        
        ArrayList<Sprite2> lumehelvesteList = new ArrayList<Sprite2>();
        
        for (int i = 0; i < 15; i++)
        {
            Sprite2 lumehelves = new Sprite2();
            lumehelves.setImage("file:src/lumememm/lumehelves.png");
            double px = 350 * Math.random() + 50;
            double py = 0;
            lumehelves.setPosition(px,py);
            
            lumehelvesteList.add( lumehelves );
        }
        
        LongValue lastNanoTime = new LongValue( System.nanoTime() );

        IntValue score = new IntValue(0);

        new AnimationTimer()
        {
            public void handle(long currentNanoTime)
            {
                // calculate time since last update.
                double elapsedTime = (currentNanoTime - lastNanoTime.value) / 1000000000.0;
                lastNanoTime.value = currentNanoTime;
                
                // game logic
                
                lumememm.setVelocity(0,0);
                if (sisend.contains("LEFT"))
                    lumememm.addVelocity(-100,0);
                if (sisend.contains("RIGHT"))
                    lumememm.addVelocity(100,0);
                    
                lumememm.update(elapsedTime);
                
                // collision detection
                
                Iterator<Sprite2> moneybagIter = lumehelvesteList.iterator();
                while ( moneybagIter.hasNext() )
                {
                    Sprite2 moneybag = moneybagIter.next();
                    if ( lumememm.intersects(moneybag) )
                    {
                        moneybagIter.remove();
                        score.value++;
                    }
                }
                
                // render
                
                gc.clearRect(0, 0, 512,512);
                lumememm.render( gc );
                
                for (Sprite2 moneybag : lumehelvesteList )
                    moneybag.render( gc );

                String pointsText = "Punkte: " + (100 * score.value);
                gc.fillText( pointsText, 360, 36 );
                gc.strokeText( pointsText, 360, 36 );
            }
        }.start();

        aken.show();
    }
}