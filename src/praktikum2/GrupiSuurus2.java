package praktikum2;

import lib.TextIO;

public class GrupiSuurus2 {

	public static void main(String[] args) {
		
		System.out.println("Sisesta inimeste arv:");
		int arv = TextIO.getlnInt();
		
		System.out.println("Sisesta grupi suurus:");
		int grupp = TextIO.getlnInt();
		
//		System.out.println("Gruppe saab moodustada: " + int gruppe = arv/grupp);
//		System.out.println("Üle jääb " + int yle = arv%grupp + " inimest");
				
		int gruppe = arv/grupp;
		int yle = arv%grupp;
		
		System.out.println("Gruppe saab moodustada: " + gruppe);
		System.out.println("Üle jääb " + yle + " inimest");	

	}

}