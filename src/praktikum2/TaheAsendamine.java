package praktikum2;

import lib.TextIO;

public class TaheAsendamine {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Kirjuta tekst:");
		String tekst = TextIO.getln();
		
		String asendus = tekst.replace('a', '_');
		
		System.out.println("Tekst muudetud kujul: " + asendus);

		
	}

}
