package praktikum2;

import lib.TextIO;

public class GrupiSuurus {

	public static void main(String[] args) {
		int arv;
		int grupp;
		
		System.out.println("Sisesta inimeste arv:");
		
		System.out.println("Sisesta grupi suurus:");
		
		arv = TextIO.getlnInt();
		grupp = TextIO.getlnInt();
		
		int gruppe = arv/grupp;
		int yle = arv%grupp;
		
		System.out.println("Gruppe saab moodustada: " + gruppe);
		System.out.println("Üle jääb " + yle + " inimest");
				
				

	}

}
