package praktikum4;


import lib.TextIO;

public class ParooliKusimine {

	public static void main(String[] args) {
		
		String parool = "Tere"; //String - objektid suure tähega, objektidel on meetodid; ei saa topeltvõrdusmärgiga võrrelda!
		
		System.out.println("Palun sisesta parool:");
		String sisestatudParool = TextIO.getln();
		
//		if(Objects.equals(parool, sisestatudParool)){
//			System.out.println("Õige parool!");
//		}	
//		else {
//			System.out.println("Vale parool!");
//		}
		
		if(sisestatudParool.equals(parool)) {
			System.out.println("Õige parool!");
		}	
		else {
			System.out.println("Vale parool!");
		}
	

	}

}
