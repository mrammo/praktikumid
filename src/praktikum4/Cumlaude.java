package praktikum4;

import lib.TextIO;

public class Cumlaude {

	public static void main(String[] args) {
		
		
		System.out.println("Sisesta keskmine hinne:");
		float keskhinne = TextIO.getlnFloat();
		if(keskhinne < 0 || keskhinne > 5){
			System.out.println("Vigane keskmine hinne");
			return;
		}
		
		System.out.println("Sisesta lõputöö hinne:");
		int loputoo = TextIO.getlnInt();
		if(loputoo < 0 || loputoo > 5){
			System.out.println("Ei ole korrektne lõputöö hinne");
			return; 
			// System.exit(0);
		}
		
		if(keskhinne > 4.5 && loputoo == 5){
			System.out.println("Jah, saad cum laude diplomile!");
		}
		else {
			System.out.println("Ei saa!");
		}
		

	}

}
