package praktikum4;

import lib.TextIO;

public class Tehisintellekt {

	public static void main(String[] args) {
		
		System.out.println("Sisesta kaks vanust");
		
		int vanus1 = 0;
		while (true) {
			vanus1 = TextIO.getlnInt();
			if (vanus1 < 0) {
				System.out.println("Vanus ei saa olla negatiivne, sisesta uuesti!");
			} else {
				break;
			}
		}
		
		int vanus2 = 0;
		while (true) {
			vanus2 = TextIO.getlnInt();
			if (vanus2 < 0) {
				System.out.println("Vanus ei saa olla negatiivne, sisesta uuesti!");
			} else {
				break;
			}
		}
		
//		int vanus1 = TextIO.getlnInt();
//		if(vanus1 < 0){
//			System.out.println("Vanus ei saa olla negatiivne!");
//			return;
//		}
//		
//		int vanus2 = TextIO.getlnInt();
//		if(vanus2 < 0){
//			System.out.println("Vanus ei saa olla negatiivne!");
//			return;
//		}
		
		if(Math.abs(vanus2 - vanus1) >= 5 && Math.abs(vanus2 - vanus1) < 10){
			System.out.println("Vanuste vahe üle 5 aasta");
		}
			
		else if(Math.abs(vanus2 - vanus1) >= 10){
			System.out.println("Vanuste vahe üle 10 aasta");
		}
			
		else if(Math.abs(vanus2 - vanus1) < 5){
			System.out.println("Vanuste vahe alla 5 aasta");
		}

	}

}
