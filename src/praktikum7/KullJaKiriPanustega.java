package praktikum7;

import praktikum6.Meetodid;

public class KullJaKiriPanustega {

	public static void main(String[] args) {
		
		int kasutajaRaha = 100;
		
		
		while (true) {
			
			int maxPanus = Math.min(25, kasutajaRaha);
			int panus = Meetodid.kasutajaSisestus("Palun sisesta panus (maksimaalselt " + maxPanus + ")", 0, maxPanus);
			kasutajaRaha -= panus;	
			
			int myndiVise = Meetodid.suvalineArv(0, 1); // 0 kull, 1 kiri
			
			System.out.println("Kasutaja panustas: " + panus);
			System.out.println("Arvuti mündivise: " + myndiVise);
			
			if (myndiVise == 1) {
				System.out.println("Tuli kiri, saad topelt tagasi!");
				kasutajaRaha += 2 * panus;
			} else {
				System.out.println("Tuli kull, ei saa midagi!");
			}
			System.out.println("Kasutaja raha alles: " + kasutajaRaha);
			
			if (kasutajaRaha <= 0) {
				System.out.println("Raha otsas!");
				break;
			}
		}


	}

}
