package praktikum7;

import lib.TextIO;

public class TagurpidiSona {

	public static void main(String[] args) {
		
		System.out.println("Kirjuta sõna: ");
		String sona = TextIO.getlnString();
		
		System.out.println(tagurpidi(sona));

	}
	
	public static String tagurpidi(String oigetpidi) {
		
		String tagurpidi = "";
		
		for (int i = oigetpidi.length() - 1; i >= 0 ; i--) {
			tagurpidi += oigetpidi.charAt(i);
			
		}
		
		return tagurpidi;
	}

}
