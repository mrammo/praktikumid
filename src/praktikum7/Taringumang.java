package praktikum7;

import praktikum6.Meetodid;

public class Taringumang {

	public static void main(String[] args) {
		
		int kasutajaRaha = 10;
		
		
		while (true) {
			
			int millelePanustab = Meetodid.kasutajaSisestus(1, 6);
			
			int maxPanus = Math.min(25, kasutajaRaha);
			int panus = Meetodid.kasutajaSisestus("Palun sisesta panus (maksimaalselt " + maxPanus + ")", 0, maxPanus);
			kasutajaRaha -= panus;	
			
			int taringuVise = Meetodid.suvalineArv(1, 6); 
			
			System.out.println("Kasutaja panustas: " + panus);
			System.out.println("Täringuvise: " + taringuVise);
			
			if (taringuVise == millelePanustab) {
				System.out.println("Tuli sama arv, saad raha 6-kordselt tagasi!");
				kasutajaRaha += 6 * panus;
			} else {
				System.out.println("Tuli erinev arv, ei saa midagi!");
			}
			System.out.println("Kasutaja raha alles: " + kasutajaRaha);
			
			if (kasutajaRaha <= 0) {
				System.out.println("Raha otsas!");
				break;
			}
		}


	}

}
