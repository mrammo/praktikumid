package praktikum6;

public class Taringumang {

	public static void main(String[] args) {
		
		int kasutaja1 = Meetodid.suvalineArv(1, 6);
		int kasutaja2 = Meetodid.suvalineArv(1, 6);		
		int kasutajaSumma = kasutaja1 + kasutaja2;

		int arvuti1 = Meetodid.suvalineArv(1, 6);
		int arvuti2 = Meetodid.suvalineArv(1, 6);
		int arvutiSumma = arvuti1 + arvuti2;
		
		System.out.println("Kasutaja arvud on " + kasutaja1 + " ja " + kasutaja2 + ". Nende summa on " + kasutajaSumma);
		System.out.println("Arvuti arvud on " + arvuti1 + " ja " + arvuti2 + ". Nende summa on " + arvutiSumma);
	}

}
