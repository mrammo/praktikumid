package KT2;

public class Ul11 {

	   public static void main(String[] args) {
	      int[] res = veeruSummad (new int[][] { {1,2,3}, {4,5,6} }); // {5, 7, 9}
	      // YOUR TESTS HERE
	   }
	  

	   public static int[] veeruSummad(int[][] m) {
	      int[] veerud = new int[m.length];
	      for (int j = 0; j < m.length; j++) {
			veerud[j] = m[0][j];
		}
	      
	      int[] summa = new int[veerud.length];
	      for (int i = 0; i < veerud.length; i++) {
			for (int j = 0; j < veerud.length; j++) {
				summa[j] = veerud[i];
			}
		}

	      return summa;
	   }

	}

// http://stackoverflow.com/questions/21641849/summing-up-each-row-and-column-in-java

