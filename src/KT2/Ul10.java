package KT2;

public class Ul10 {

	   public static void main (String[] args) {
	      System.out.println (result (new double[]{0., 1., 2., 6., 4.}));
	      // YOUR TESTS HERE
	   }

	   public static double result (double[] marks) {
		   double vaiksem = Integer.MAX_VALUE;
		   double suurim = Integer.MIN_VALUE;
		   
		   for (int i = 0; i < marks.length; i++) {
			   if (vaiksem > marks[i])
				   vaiksem = marks[i];	
		}
		   for (int i = 0; i < marks.length; i++) {
			   if (suurim < marks[i])
				   suurim = marks[i];	
		}
		   double summa = 0;
		   for (int i = 0; i < marks.length; i++) {
			   summa += marks[i];   
		   }
		   double summa2 = summa - suurim - vaiksem;
		  

		   double aritmeetilineKeskmine = summa2 / (marks.length-2);
		   
	      return aritmeetilineKeskmine;  // TODO!!! YOUR PROGRAM HERE
	   }

	}